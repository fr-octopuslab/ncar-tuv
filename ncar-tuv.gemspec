#--
# Copyright (C) 2021 - Octopus Lab SAS
# All rights are described by the GPLv3 license present in the file /LICENSE available in the
# original repository of this file or [here](https://www.gnu.org/licenses/gpl-3.0.fr.html).
#++
# frozen_string_literal: true

require_relative 'lib/tuv/version'

::Gem::Specification.new do |s|
  s.name        = 'ncar-tuv'
  s.version     = ::Tuv::VERSION
  s.summary     = 'Base Ruby Library Project'
  s.description = 'This is an example of ruby library with gemspec and a working .gitlab-ci.yml' \
                  "deploying doc to Gitlab's Pages"
  s.authors     = ['Roland Laurès']
  s.email       = 'roland@octopuslab.fr'
  s.files       = ::Dir.glob('{bin,lib}/**/*') + %w[LICENSE README.md]
  # s.executables = ...
  s.homepage    = 'https://rubygems.org/gems/base-ruby-library-project'
  s.require_path = 'lib'
  s.license = 'GPL-3.0'
  s.required_ruby_version = '>= 2.5'

  s.metadata = {
    'bug_tracker_uri' => 'https://gitlab.com/fr-octopuslab/ncar-tuv/-/issues',
    'changelog_uri' => 'https://gitlab.com/fr-octopuslab/ncar-tuv/-/blob/main/CHANGELOG.md',
    'documentation_uri' => "https://fr-octopuslab.gitlab.io/ncar-tuv/#{s.version}",
    'homepage_uri' => 'https://gitlab.com/fr-octopuslab/ncar-tuv',
    'rubygems_mfa_required' => 'true'
    # "mailing_list_uri"  => "https://groups.example.com/bestgemever",
    # "source_code_uri"   => "https://example.com/user/bestgemever",
    # "wiki_uri"          => "https://example.com/user/bestgemever/wiki",
    # "funding_uri"       => "https://example.com/donate"
  }

  # dependencies
  s.add_runtime_dependency('thor', '~> 1.1.0')

  # development dependencies
  s.add_development_dependency('pry', '~> 0.14')
  s.add_development_dependency('rspec', '~> 3.10')
  s.add_development_dependency('rubocop', '~> 1.20.0')
  s.add_development_dependency('rubocop-rspec', '~> 2.4')
  s.add_development_dependency('simplecov', '~> 0.21.2')
  s.add_development_dependency('yard', '~> 0.9')
end
