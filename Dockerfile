FROM ruby:3.0-bullseye

# Install build dependencies
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections && \
    apt-get update && \
    apt-get install -y gfortran make

# Copy necessary files to run bundle install
WORKDIR /ncar-tuv

COPY . ./


# Build
RUN make && gem build ncar-tuv.gemspec

# Install
RUN gem install ncar-tuv-*.gem
