# TUV ruby launcher

## Description

This project is just a wrapper to TUV application.

See
[here](https://www2.acom.ucar.edu/modeling/tropospheric-ultraviolet-and-visible-tuv-radiation-model)
and access to the original sources.

## Library usage

TODO: add working description here on how to use it.

## Contributing

Thanks to think of contributing.

We are open to contributions. Please feal free to create an issue covering what
you wanted to do and to create a PR directed to the develop branch for it to be
accepted.

We do not have clear development roadmap, because we will contribute to this
project on the need for us. This won't prevent to have time answering to your
needs, and welcome test cases or other bug reports.

Please:

- respect each others (take example to existing [code of conducts like the one
  of Atom for example](https://github.com/atom/atom/blob/master/CODE_OF_CONDUCT.md)).
- do not use issues or PR comments for anything else than the purpose of this
  project.

We will use censorship when a comment is deemed (by us - project managers) as
inappropriate (Trolling, insults or any other non-productive discussions which
does not deserve to be stored).
