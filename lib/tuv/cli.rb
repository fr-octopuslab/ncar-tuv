#--
# Copyright (C) 2021 - Octopus Lab SAS
# All rights are described by the GPLv3 license present in the file /LICENSE available in the
# original repository of this file or [here](https://www.gnu.org/licenses/gpl-3.0.fr.html).
#++
# frozen_string_literal: true

require 'thor'

module Tuv
  # The Thor command line interface code
  class Cli < Thor
    desc 'versions', 'Display current versions numbers'
    # display the versions of the current gem.
    def versions
      puts("Tuv library/cli version: #{::Tuv::VERSION}")
      puts("NCAR's TUV tool version: #{::Tuv::TUV_EXEC_VERSION}")
    end

    desc 'run', 'Run TUV command'
    # Just run NCAR TUV tool
    def run
      ::Tuv::Runner.perform
    end
  end
end
