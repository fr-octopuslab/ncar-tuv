#--
# Copyright (C) 2021 - Octopus Lab SAS
# All rights are described by the GPLv3 license present in the file /LICENSE available in the
# original repository of this file or [here](https://www.gnu.org/licenses/gpl-3.0.fr.html).
#++
# frozen_string_literal: true

require 'pathname'

module Tuv
  # The Runner class
  class Runner
    # The method to call to run TUV tool
    def self.perform
      new.perform
    end

    def perform
      Dir.chdir(File.expand_path)
    end

    private

    def to_run; end
  end
end
