#--
# Copyright (C) 2021 - Octopus Lab SAS
# All rights are described by the GPLv3 license present in the file /LICENSE available in the
# original repository of this file or [here](https://www.gnu.org/licenses/gpl-3.0.fr.html).
#++
# frozen_string_literal: true

module Tuv
  VERSION = '0.0.1'
  public_constant :VERSION
  TUV_EXEC_VERSION = '5.4.0'
  public_constant :TUV_EXEC_VERSION
end
