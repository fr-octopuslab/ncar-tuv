#--
# Copyright (C) 2021 - Octopus Lab SAS
# All rights are described by the GPLv3 license present in the file /LICENSE available in the
# original repository of this file or [here](https://www.gnu.org/licenses/gpl-3.0.fr.html).
#++
# frozen_string_literal: true

# The Tuv module
module Tuv
  # Base error class for all of the errors raised by this library.
  class Error < StandardError; end

  # Error related to bad argument given to a method.
  class ArgumentError < Error; end

  # Error whena method is not yet implemented
  class NotImplementedError < Error; end
end

require 'tuv/version'
